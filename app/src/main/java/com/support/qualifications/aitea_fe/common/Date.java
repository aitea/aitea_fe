package com.support.qualifications.aitea_fe.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Dateクラスは現在時刻情報を扱う
 */
public final class Date {

    /**
     * 現在時刻( yyyy-MM-dd hh:mm:ss )を取得するクラス
     * @return 現在時刻( yyyy-MM-dd hh:mm:ss )
     */
    public static final String getNowDate(){

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = new java.util.Date(System.currentTimeMillis());
        return df.format(date);
    }
}
