package com.support.qualifications.aitea_fe.processing;

import java.util.ArrayList;

/**
 * 弱点分析を行うための演算処理をまとめたもの
 */
public final class Analyze_Calculate {

     /** いくつの回答履歴を参照するか */
    public static final int AIM_ANS_COUNT = 10;
    /**
     * 回答内容から正答率を算出する際の分子・分母を計算し、結果を返す。
     * 分母が0の場合は0を返す
     * correct_infoの大きさが0の場合は0を返す
     *
     * @param correct_info 回答内容(正解:1,不正解:0)を保持した配列
     * @return 正答率を算出する際の分母・分子
     */
    public static final double[] molec_denomin(int[] correct_info){

        double[] molec_denomin = { 0.0 , 0.0 }; //[0]:分子,[1]:分母

        int ans_count = correct_info.length;   //回答数

        if( ans_count == 0 ){
            return molec_denomin;
        }

        for(int i = 0; i < ans_count ; i++){

            double infr = get_infuence_value(i,ans_count); //影響度の計算

            molec_denomin[0] += infr * correct_info[i];
            molec_denomin[1] += infr;
        }

        return molec_denomin;
    }

    /**
     * 正答率計算に用いる、影響度を算出し、結果を返すメソッド
     * @param index 何番目の影響度か
     * @param ans_count いくつの回答を扱っているか
     * @return 影響度
     */
    private static final double get_infuence_value( int index , int ans_count){

        double infr;

        if( index == 0 ){
            infr = 1;
        }else{
            infr = get_infuence_value(index - 1, ans_count) - (1 / (double)ans_count);
        }

        return infr;
    }

    /**
     * 出題確率を計算するメソッド
     * count_rが0の場合0を返す
     *
     * @param r 順位
     * @param count_r 順位に該当する正答率の個数
     * @param t 順位rの総数
     * @return 出題確率
     */
    private static final double get__set_q_rate(int r, int count_r, int t){

        double total = 0;

        for( int i = 1; i <= t; i ++ ){
            total += 1 / (double)i;
        }

        //0除算防止
        if(count_r == 0){
            return 0;
        }

        return ( 1 / ( r * total ) ) / (double)count_r;
    }


    /**
     * 出題確率を計算して引数q_rate_infosに格納する
     * @param q_rate_infos 出題確率格納対象のデータ型配列
     */
    public static final  void calc_set_q_rate_infos(ArrayList<q_rate_info> q_rate_infos){

        //順位の総数
        final int t = q_rate_infos.get(q_rate_infos.size()- 1).getRank();

        //順位
        int r = t;

        //順位に該当するジャンルの個数
        int count_r = q_rate_infos.get(q_rate_infos.size() - 1).getRank_repetedCount();

        for( int i = q_rate_infos.size() - 1; i > 0; i-- ){

            //出題確率をセット
            q_rate_infos.get(i).setSet_q_rate(Analyze_Calculate.get__set_q_rate(r, count_r, t));

            //異なる順位のジャンルの場合
            if( q_rate_infos.get(i).getRank() != q_rate_infos.get(i - 1).getRank() ) {
                r = q_rate_infos.get(i - 1).getRank();
                count_r = q_rate_infos.get(i - 1).getRank_repetedCount();
            }
        }

        //出題確率をセット(index:0)
        q_rate_infos.get(0).setSet_q_rate(Analyze_Calculate.get__set_q_rate(r, count_r, t));

    }

    /**
     * 正答率の順位(昇順)を計算して引数q_rate_infosに格納する
     * @param q_rate_infos 順位と重複順位数の格納対象のデータ型配列
     */
    public static final void set_rank(ArrayList<q_rate_info> q_rate_infos){

        //順位
        int rank = 1;

        //重複順位数( 同じ順位を持つジャンルの数 )
        int rank_repetedCount = 1;

        for( int i = 1; i < q_rate_infos.size(); i++ ){

            //同じ順位の場合
            if( q_rate_infos.get(i - 1).getCorrect_rate() == q_rate_infos.get(i).getCorrect_rate() ){
                rank_repetedCount++;
            }
            else{
                rank++;
                q_rate_infos.get(i - 1).setRank_repetedCount(rank_repetedCount);
                rank_repetedCount = 1;
            }

            //順位をセット
            q_rate_infos.get(i).setRank(rank);
        }

        //順位をセット(index:配列の最後の要素)
        q_rate_infos.get(q_rate_infos.size() - 1).setRank_repetedCount(rank_repetedCount);
    }
}
