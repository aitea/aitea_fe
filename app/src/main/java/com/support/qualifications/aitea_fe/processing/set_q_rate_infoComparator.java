package com.support.qualifications.aitea_fe.processing;

/**
 *　正答率を昇順にソートするための比較クラス
 */
public class set_q_rate_infoComparator implements java.util.Comparator {

    @Override
    /**
     * ２つのreturnの値( 1 と -1 )を反転させれば降順になる
     */
    public int compare(Object lhs, Object rhs) {
        if(((q_rate_info)lhs).getCorrect_rate() > ((q_rate_info) rhs).getCorrect_rate()){
            return 1;
        }
        else if(((q_rate_info)lhs).getCorrect_rate() < ((q_rate_info) rhs).getCorrect_rate()){
            return -1;
        }
        return 0;
    }
}