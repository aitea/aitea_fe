package com.support.qualifications.aitea_fe.processing;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html;

/**
 * htmlタグを解析してイメージを返せるようにする
 * @author S.Kamba
 *
 */
public class ImageGetterImpl implements Html.ImageGetter {

    Context mContext = null;
    private int height = 0;

    public ImageGetterImpl(Context context){
        mContext = context;
    }

    /**
     * 画像の高さを受け取るメソッド
     * @param height TextViewから取得したfontsize
     */
    public void setHeight(int height){
        this.height = height;
    }

    @Override
    public Drawable getDrawable(String source) {
        // sourceにimgタグの src=""で指定した内容が渡される

        Resources res = mContext.getResources();
        // 画像のリソースIDを取得
        int id = res.getIdentifier(source, "drawable", mContext.getPackageName());
        try{
            // リソースIDから Drawable のインスタンスを取得
            Drawable d = res.getDrawable(id);
            // 取得した元画像のサイズを取得し、必要なら、表示画像のサイズを調整する
            int w = d.getIntrinsicWidth();
            int h = d.getIntrinsicHeight();

            int tmp_height = this.height;
            int weight = (int)(tmp_height * (w/h));

            if(source.indexOf("symbol")!=-1){
                weight = (int)(weight*0.8f);
                tmp_height = (int)(tmp_height*0.8f);
            }
            d.setBounds(0, 0, weight, tmp_height);
            return d;
        }catch (Resources.NotFoundException e){
           return null;
        }

    }
}
