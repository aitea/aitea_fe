package com.support.qualifications.aitea_fe.application;

import android.app.Application;

import com.beardedhen.androidbootstrap.TypefaceProvider;

/**
 * Bootstrapを使用する際に必要なクラス
 */
public class Bootstrap extends Application {
    @Override public void onCreate() {
        super.onCreate();
        TypefaceProvider.registerDefaultIconSets();
    }
}
