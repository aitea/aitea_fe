package com.support.qualifications.aitea_fe.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.AiteaMode;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.server_send_data;
import com.support.qualifications.aitea_fe.processing.DataSender;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * トップ画面
 */
public class Activity_Main extends Activity_Base {

    /** 起動したかどうかを保持するためのプリファレンスのキー */
    public static final String INITSTATE = "com.support.qualifications.aitea_fe.InitState";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //インテントフラグを設定
        setIntentFlag(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        //サーバへの送信を許可している
        if(getPermissionSendToServer()) {
            //サーバにデータ送信
            send_to_server();
        }

        //ボタンのクリックリスナーをセット
        setButtonsClickListener();

        //ツールバーをセット
        setToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //初回起動時のみ表示するアラート表示
        showAlertOnlyFirstBoot();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * Toolbarをセットする
     */
    private void setToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setNavigation(toolbar);
    }


    /**
     * サーバへの送信許可をセット
     * @param state true:許可、false:許可しない
     */
    private void setPermissionSendToServer(boolean state) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putBoolean(getString(R.string.key_permissionSendToServer), state).commit();
    }
    /**
     * サーバへの送信許可を取得
     * @return 許可:true,許可しない:false
     */
    private boolean getPermissionSendToServer() {
        // 読み込み
        boolean state;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        state = sp.getBoolean(getString(R.string.key_permissionSendToServer) , false );
        return state;
    }

    /**
     * 一度でも起動したかどうかを保持
     * @param state 起動した:true,起動してない:false
     */
    private void setBootedState(boolean state) {
        // SharedPreferences設定を保存
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit().putBoolean(INITSTATE, state).commit();
    }

    /**
     * 一度でも起動したかどうかの情報を取得
     * @return 起動した:true,起動してない:false
     */
    private boolean getBootedState() {
        // 読み込み
        boolean state;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        state = sp.getBoolean(INITSTATE , false );
        return state;
    }

    /**
     * 初回起動時のみサーバへの送信許可を確認する
     * アラートを表示する
     */
    private void showAlertOnlyFirstBoot(){

        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.alert_title_server_send_permission));
        alertDialog.setMessage(getString(R.string.alert_message_server_send_permission));

        alertDialog.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //初回表示完了
                setBootedState(true);
                //サーバへの送信を許可する
                setPermissionSendToServer(true);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //初回表示完了
                setBootedState(true);
                //サーバへの送信を許可しない
                setPermissionSendToServer(false);
            }
        });

        // 一度も起動していないときのみ表示
        if(!getBootedState()){
            alertDialog.create();
            alertDialog.show();
        }
    }

    /**
     * ボタンのクリックリスナーをセットする
     */
    private void setButtonsClickListener(){

        // aitea
        findViewById(R.id.mode_aitea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //問題回答画面へ
                goAiteaMode();
            }
        });

        // 模擬試験
        findViewById(R.id.mode_mockexam).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goTrialExam();
            }
        });

        //ジャンル別
        findViewById(R.id.button_mode_dictionary_genre).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goDictionaryGenre();
            }
        });

        //年度別
        findViewById(R.id.button_mode_dictionary_year).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goDictionary_Year(AiteaMode.YEAR);
            }
        });

        // 復習
        findViewById(R.id.mode_review).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goReview();
            }
        });

        //お気に入り
        findViewById(R.id.mode_favorite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goFavorite();
            }
        });
    }


    /**
     * サーバ送信情報テーブルの全てのデータを取得する
     * @return サーバ送信情報データ
     */
    private server_send_data[] get_send_data(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.get_server_send_info(), null);
        server_send_data[] serverSendDatas = new server_send_data[c.getCount()];

        int i=0;
        while (c.moveToNext()){
            serverSendDatas[i] = new server_send_data();
            serverSendDatas[i].setExam_category(c.getString(c.getColumnIndex(DB_ColumnNames.POST_DATA.EXAM_CATEGORY)));
            serverSendDatas[i].setQuestion_id(c.getString(c.getColumnIndex(DB_ColumnNames.POST_DATA.Q_ID)));
            serverSendDatas[i].setUser_answer(c.getString(c.getColumnIndex(DB_ColumnNames.POST_DATA.USER_ANSWER)));
            serverSendDatas[i].setCorrect_mistake(c.getString(c.getColumnIndex(DB_ColumnNames.POST_DATA.CORRECT_MISTAKE)));
            i++;
        }
        c.close();
        db.close();
        return serverSendDatas;
    }

    /**
     * サーバ送信情報をサーバに送信する
     */
    private void send_to_server(){
        // Pingによる疎通確認
        if(ping()) {
            //サーバ送信情報を取得
            server_send_data[] send_data = get_send_data();
            //サーバへ送信
            data_send(createServer_send_data(send_data));
        }
    }

    /**
     * サーバ送信情報配列をArrayListに詰め替え返す
     * @param send_data サーバー送信情報配列
     * @return サーバー送信情報ArrayList
     */
    private ArrayList<server_send_data> createServer_send_data(server_send_data[] send_data) {

        if (send_data.length == 0) {
            return null;
        }
        ArrayList<server_send_data> dataList = new ArrayList<server_send_data>();

        for (int i = 0; i < send_data.length; i++) {
            dataList.add(send_data[i]);
        }
        return dataList;
    }

    /**
     * サーバー送信データをサーバに送信する
     * @param dataList サーバ送信データ
     */
    private void data_send(ArrayList<server_send_data> dataList){

        if( dataList == null ){
            return;
        }

        //JSON形式に変換しjsonListに格納する
        List<NameValuePair> jsonList = setJson("input", dataList);

        //送信プロセスの生成
        Uri.Builder builder = new Uri.Builder();

        //DataSenderタスクを生成し、jsonListを渡す
        DataSender sender = new DataSender( jsonList);

        //送信プロセスの実行
        sender.execute(builder);

        //送信が成功したと判断した場合、サーバ送信情報テーブルの全レコードを削除
        delete_tbl_info(DB_ColumnNames.SERVER_SEND_INFO.TABLE_NAME);
    }


    /**
     * 引数のサーバ送信情報データリストをJSON形式に変換しjsonListに格納する
     * @param objName paramsのオブジェクト名
     * @param valueList サーバ送信情報データリスト
     * @return
     */
    private List<NameValuePair> setJson (String objName, ArrayList<server_send_data> valueList) {

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        try {
            /* JSONデータのフィールド名と要素を詰める */
            JSONArray array = new JSONArray();
            for(int i=0; i<valueList.size(); i++){
                JSONObject object = new JSONObject();
                object.put(DB_ColumnNames.POST_DATA.EXAM_CATEGORY, valueList.get(i).getExam_category());
                object.put(DB_ColumnNames.POST_DATA.Q_ID, valueList.get(i).getQuestion_id());
                object.put(DB_ColumnNames.POST_DATA.USER_ANSWER, valueList.get(i).getUser_answer());
                object.put(DB_ColumnNames.POST_DATA.CORRECT_MISTAKE, valueList.get(i).getCorrect_mistake());
                array.put(object);
            }
            /* paramsに格納する */
            BasicNameValuePair params_value = new BasicNameValuePair(objName, array.toString());
            params.add(params_value);
            return params;

        } catch (Exception e) {
            return null;
        }
    }

    /**
     * pingを送信する
     * @return 送信成功:true , 送信失敗:false
     */
    private static boolean ping(){
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        try{
            process = runtime.exec("ping -c 1 -w 1 " + "aiteaz.com");
            process.waitFor();
        }catch(Exception e){}
        int exitVal = process.exitValue();
        if(exitVal == 0)return true;
        else return false;
    }
}
