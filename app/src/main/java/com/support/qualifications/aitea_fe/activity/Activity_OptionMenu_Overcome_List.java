package com.support.qualifications.aitea_fe.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.question_info_data;
import com.support.qualifications.aitea_fe.processing.ScrollValue;

import java.util.ArrayList;

public class Activity_OptionMenu_Overcome_List extends Activity_Base {

    /** モードID */
    private int mode_id;

    /** リストビューのスクロール位置 */
    public static ScrollValue scrollValue =  new ScrollValue();

    /** 問題IDリスト */
    private String[] qid_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmenu_overcome_list);

        //自動スクロール位置初期化
        scrollValue.setScrollposition(0);

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_overcome_list));

        //intentからデータ取得
        getIntentValue();

        setButtonsClickListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //ListViewセット
        setListView(getUsers());

        final ListView myListView = (ListView) findViewById(R.id.listview_mode_overcome);
        myListView.post(new Runnable() {
            @Override
            public void run() {
                myListView.smoothScrollToPositionFromTop(scrollValue.getScrollposition(), 0, 400);
            }
        });
    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }


    /**
     * Intentからデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
        ((TextView)findViewById(R.id.mode_overcome_title)).setText(intent.getStringExtra(IntentKeyword.TOOLBER_TITLE));
    }


    /**
     * ボタンのクリック処理を設定する
     */
    private void setButtonsClickListener(){
        findViewById(R.id.overcome_reset_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetAlertShow();
            }
        });
    }


    /**
     * 克服リストから問題をすべて削除するかどうか確認し、
     * "はい"を選択すると、データベースの克服フラグをすべておろす
     */
    private void ResetAlertShow(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_overcome_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_overcome_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downAllOvercomeFlag();
                setListView(getUsers());
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(){

        ArrayList<User> users = new ArrayList<>();

        //リストViewを表示するために必要な情報を取得
        question_info_data[] q_i_data = get_list_q_i_info_forOvercome();

        for (int i = 0; i < q_i_data.length; i++) {
            User user = new User();
            user.setQ_info(q_i_data[i].getYear() + " " + q_i_data[i].getTurn() + " 問 " + String.format("%02d", q_i_data[i].getQ_no()));
            user.setQ_summary(q_i_data[i].getQ_name());
            users.add(user);
        }
        return users;
    }


    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users ){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        final ListView myListview = (ListView)findViewById(R.id.listview_mode_overcome);
        myListview.setEmptyView(findViewById(R.id.mode_overcome_empty));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {
                //選択した問題回答画面へ
                goSelectQuestionActivity(position);
            }
        });
    }


    /**
     * ListViewで選択した問題の回答画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectQuestionActivity(int position){

        Intent intent = new Intent(Activity_OptionMenu_Overcome_List.this, Activity_Question.class);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
        intent.putExtra(IntentKeyword.Q_ID_LIST, qid_data);
        startActivity(intent);
    }

    /**
     * 指定問題IDの克服フラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setOvercomeFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 0));
        }

        db.close();
    }


    /**
     * 克服一覧に必要な問題情報を取得する
     * @return 問題情報
     */
    private question_info_data[] get_list_q_i_info_forOvercome(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_list_q_i_info_forOvercome(), null);
        qid_data = new String[c.getCount()];
        question_info_data[] questionInfoDatas = new question_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            qid_data[i] = new String(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID)));
            questionInfoDatas[i] = new question_info_data();
            questionInfoDatas[i].setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoDatas[i].setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            questionInfoDatas[i].setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
            questionInfoDatas[i].setQ_name(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME)));
            i++;
        }
        c.close();
        db.close();
        return questionInfoDatas;
    }

    /**
     * 指定問題IDの克服フラグを取得する
     * @param q_id 問題ID
     * @return 克服フラグ
     */
    private boolean getOvercomeFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_overcome_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

    /**
     * 全ての克服フラグをおろす
     */
    private void downAllOvercomeFlag(){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.down_all_overcome_flag());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            final ViewHolder holder;

            if( convertview == null ){
                convertview = layoutInflater.inflate(R.layout.question_list_item_flags, parent, false);
                holder = new ViewHolder();
                holder.info = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                holder.overcome_icon = (ImageView) convertview.findViewById(R.id.icon);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.info.setBootstrapText(new BootstrapText.Builder(getContext()).addText(user.getQ_info()).build());
            holder.summary.setText(user.getQ_summary());
            holder.overcome_icon.setImageBitmap(getFlagIcon(pos));
            holder.overcome_icon.setTag(pos);
            holder.overcome_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (getOvercomeFlag(qid_data[pos])) {
                        ((ImageView) v).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_overcome_false2));
                        setOvercomeFlag(qid_data[pos], false);
                    } else {
                        ((ImageView) v).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_overcome_true));
                        setOvercomeFlag(qid_data[pos], true);
                    }
                }
            });
            return convertview;

        }
    }


    /**
     * フラグに対応するアイコンのビットマップを返す
     * @param pos アイコンの位置
     * @return ビットマップ
     */
    private Bitmap getFlagIcon(int pos){
        if(getOvercomeFlag(qid_data[pos])){
            return BitmapFactory.decodeResource(getResources(), R.drawable.icon_overcome_true);
        }else{
            return BitmapFactory.decodeResource(getResources(), R.drawable.icon_overcome_false);
        }
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** 問題情報( 問1など )を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel info;

        /** 問題名を表示するテキスト */
        TextView summary;

        /** 克服画像 */
        ImageView overcome_icon;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    private class User{

        /** 問題情報( 問1など ) */
        private String q_info;

        /** 問題名 */
        private String q_summary;

        /**
         * 問題情報を格納する
         * @param q_info 問題情報( 問1 平成**年度 *期 )
         */
        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * 問題情報を取得する
         * @return 問題情報
         */
        public String getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }
    }


}
