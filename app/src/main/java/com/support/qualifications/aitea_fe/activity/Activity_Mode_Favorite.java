package com.support.qualifications.aitea_fe.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapText;
import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.question_info_data;
import com.support.qualifications.aitea_fe.processing.ScrollValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Activity_Mode_Favorite extends Activity_Base implements CompoundButton.OnCheckedChangeListener {

    /** モードID */
    private int mode_id;

    /** リストビューのスクロール位置 */
    public static ScrollValue scrollValue =  new ScrollValue();

    /** ランダムモード(true:ランダムモード,false:通常モード) */
    private boolean isRandomMode;

    /** 問題IDリスト */
    private String[] qid_data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_favorite);

        //自動スクロール位置初期化
        scrollValue.setScrollposition(0);

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_mode_favorite));

        //intentからデータ取得
        getIntentValue();

        //ランダムスイッチにチェンジリスナーをセット
        ((Switch)findViewById(R.id.mode_favorite_random_switch)).setOnCheckedChangeListener(this);

        setButtonsClickListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //ListViewセット
        setListView(getUsers());

        //今まで解いていた問題の場所まで自動スクロール
        if(!isRandomMode) {
            final ListView myListView = (ListView) findViewById(R.id.listview_mode_favorite);
            myListView.post(new Runnable() {
                @Override
                public void run() {
                    myListView.smoothScrollToPositionFromTop(scrollValue.getScrollposition(), 0, 400);
                }
            });
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * Intentからデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
        ((TextView)findViewById(R.id.mode_favorite_title)).setText(intent.getStringExtra(IntentKeyword.TOOLBER_TITLE));
    }

    /**
     * ボタンのクリック処理を設定する
     */
    private void setButtonsClickListener(){
        findViewById(R.id.favorite_reset_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetAlertShow();
            }
        });
    }

    /**
     * お気に入りリストから問題をすべて削除するかどうか確認し、
     * "はい"を選択すると、データベースのおきにいりフラグをすべておろす
     */
    private void ResetAlertShow(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_favorite_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_favorite_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downAllFavoriteFlag();
                setListView(getUsers());
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    /**
     * ListViewに表示するデータの定義クラス(User)のArrayListを作成し
     * その結果を返す
     * @return UserのArrayList
     */
    private ArrayList<User> getUsers(){

        ArrayList<User> users = new ArrayList<>();

        //リストViewを表示するために必要な情報を取得
        question_info_data[] q_i_data = get_list_q_i_info_forFavorite();

        for (int i = 0; i < q_i_data.length; i++) {
            User user = new User();
            user.setQ_info(q_i_data[i].getYear() + " " + q_i_data[i].getTurn() + " 問 " + String.format("%02d", q_i_data[i].getQ_no()));
            user.setQ_summary(q_i_data[i].getQ_name());
            users.add(user);
        }
        return users;
    }

    /**
     * ListViewの設定を行う
     * @param users　ListViewに表示するデータ
     */
    private void setListView( ArrayList<User> users ){

        UserAdapter adapter = new UserAdapter(this, 0, users);
        final ListView myListview = (ListView)findViewById(R.id.listview_mode_favorite);
        myListview.setEmptyView(findViewById(R.id.mode_favorite_empty));
        myListview.setAdapter(adapter);
        myListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> parent,
                    View view, //タップされたビュー
                    int position, //何番目？
                    long id //View id
            ) {
                //選択した問題回答画面へ
                goSelectQuestionActivity(position);
            }
        });
    }

    /**
     * 問題IDリストをシャッフルする
     * 選択した問題の問題IDは先頭に配置する
     * @param position 選択した問題の場所
     * @return シャッフルした問題IDリスト
     */
    private String[] getShuffle_qid_data(int position){
        //配列からlistへ変換
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(qid_data));
        //選択した問題のq_idを退避
        String first_q_id = list.get(position);
        list.remove(position);
        //リストの並びをシャッフル
        Collections.shuffle(list);
        //退避したq_idを先頭に追加
        list.add(0, first_q_id);
        //シャッフルしたデータを返す
        return (String[])list.toArray(new String[list.size()]);
    }

    /**
     * ListViewで選択した問題の回答画面に画面遷移する処理を行う
     * @param position 選択した問題の場所
     */
    private void goSelectQuestionActivity(int position){

        Intent intent = new Intent(Activity_Mode_Favorite.this, Activity_Question.class);

        intent.putExtra(IntentKeyword.MODE_ID, mode_id);

        //ランダムモード
        if(isRandomMode) {
            intent.putExtra(IntentKeyword.SELECT_Q_ID, 0);
            intent.putExtra(IntentKeyword.Q_ID_LIST, getShuffle_qid_data(position));
        }else{
            intent.putExtra(IntentKeyword.SELECT_Q_ID, position);
            intent.putExtra(IntentKeyword.Q_ID_LIST, qid_data);
        }
        startActivity(intent);
    }
    /**
     * 指定問題IDのお気に入りフラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setFavoriteFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id, 1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id, 0));
        }

        db.close();
    }

    /**
     * お気に入りモードに必要な問題情報を取得する
     * @return 問題情報
     */
    private question_info_data[] get_list_q_i_info_forFavorite(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_list_q_i_info_forFavorite(), null);
        qid_data = new String[c.getCount()];
        question_info_data[] questionInfoDatas = new question_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            qid_data[i] = new String(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID)));
            questionInfoDatas[i] = new question_info_data();
            questionInfoDatas[i].setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            questionInfoDatas[i].setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            questionInfoDatas[i].setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
            questionInfoDatas[i].setQ_name(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NAME)));
            i++;
        }
        c.close();
        db.close();
        return questionInfoDatas;
    }
    /**
     * 指定問題IDのお気に入りフラグを取得する
     * @param q_id 問題ID
     * @return お気に入りフラグ
     */
    private boolean getFavoriteFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_favorite_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

    /**
     * 全てのお気に入りフラグをおろす
     */
    private void downAllFavoriteFlag(){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.down_all_favorite_flag());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(isChecked == true){
            //ランダムモードon
            isRandomMode = true;
        }else{
            //ランダムモードoff
            isRandomMode = false;
        }
    }

    /**
     * ListViewにUser型データをセットする際に用いるアダプタ
     */
    public class UserAdapter extends ArrayAdapter<User> {

        private LayoutInflater layoutInflater;

        public UserAdapter(Context c, int id, ArrayList<User> users) {
            super(c, id, users);
            this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        /* リストをスクロールして、新たにリストが表示されるときに呼ばれる */
        @Override
        public View getView(int pos, View convertview, ViewGroup parent){

            final ViewHolder holder;

            if( convertview == null ){
                convertview = layoutInflater.inflate(R.layout.question_list_item_flags, parent, false);
                holder = new ViewHolder();
                holder.info = (com.beardedhen.androidbootstrap.BootstrapLabel) convertview.findViewById(R.id.questionInfo);
                holder.summary = (TextView) convertview.findViewById(R.id.questionsummary);
                holder.favorite_icon = (ImageView) convertview.findViewById(R.id.icon);
                convertview.setTag(holder);
            }else{
                holder = (ViewHolder)convertview.getTag();
            }
            User user = (User)getItem(pos);
            holder.info.setBootstrapText(new BootstrapText.Builder(getContext()).addText(user.getQ_info()).build());
            holder.summary.setText(user.getQ_summary());
            holder.favorite_icon.setImageBitmap(getFlagIcon(pos));
            holder.favorite_icon.setTag(pos);
            holder.favorite_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (getFavoriteFlag(qid_data[pos])) {
                        ((ImageView) v).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_favorite_false2));
                        setFavoriteFlag(qid_data[pos], false);
                    } else {
                        ((ImageView) v).setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_favorite_true));
                        setFavoriteFlag(qid_data[pos], true);
                    }
                }
            });
            return convertview;

        }
    }

    /**
     * フラグに対応するアイコンのビットマップを返す
     * @param pos アイコンの位置
     * @return ビットマップ
     */
    private Bitmap getFlagIcon(int pos){
        if(getFavoriteFlag(qid_data[pos])){
            return BitmapFactory.decodeResource(getResources(), R.drawable.icon_favorite_true);
        }else{
            return BitmapFactory.decodeResource(getResources(), R.drawable.icon_favorite_false);
        }
    }

    /**
     * ListViewが持つView郡の定義クラス
     */
    static class ViewHolder{

        /** 問題情報( 問1など )を表示するテキスト */
        com.beardedhen.androidbootstrap.BootstrapLabel info;

        /** 問題名を表示するテキスト */
        TextView summary;

        /** お気に入り画像 */
        ImageView favorite_icon;
    }

    /**
     * ListViewに表示するデータの定義クラス
     */
    private class User{

        /** 問題情報( 問1など ) */
        private String q_info;

        /** 問題名 */
        private String q_summary;

        /**
         * 問題情報を格納する
         * @param q_info 問題情報( 問1 平成**年度 *期 )
         */
        public void setQ_info(String q_info) {
            this.q_info = q_info;
        }

        /**
         * 問題名を格納する
         * @param q_summary 問題名
         */
        public void setQ_summary(String q_summary) {
            this.q_summary = q_summary;
        }

        /**
         * 問題情報を取得する
         * @return 問題情報
         */
        public String getQ_info() {
            return q_info;
        }

        /**
         * 問題名を取得する
         * @return 問題名
         */
        public String getQ_summary() {
            return q_summary;
        }
    }

}
