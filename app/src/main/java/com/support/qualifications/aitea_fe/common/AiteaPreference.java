package com.support.qualifications.aitea_fe.common;

/**
 * 扱うプリファレンス名を
 */
public final class AiteaPreference {

    /** Aitea共通で扱うプリファレンス名 */
    public final static String PREFNAME = "AiteaPref";

    /** 克服説明表示フラグを保持するためのプリファレンスのキー */
    public static final String OVERCOMEALERTFLAG = "com.support.qualifications.aitea_fe.OVERCOMEALERTFLAG";
}
