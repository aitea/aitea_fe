package com.support.qualifications.aitea_fe.common;

/**
 * IntentKeywordクラスはアプリ間でデータを受け渡す際の、合言葉として扱う
 * 2箇所以上のActivityで利用している同じ用途の合言葉を使用している場合はここに定義している
 */
public  final class IntentKeyword {

    /** question_idの配列を送受信する際に用いる */
    public final static String Q_ID_LIST = "com.support.qualifications.aitea_fe.Q_ID_LIST";

    /** リストなどで選択した問題のquestion_idを取得するための、配列内のポジションを送受信する際に用いる（Q_ID_LISTとセットで用いる） */
    public final static String SELECT_Q_ID = "com.support.qualifications.aitea_fe.SELECT_Q_ID";

    /** モードの情報を送受信する際に用いる */
    public final static String MODE_ID = "com.support.qualifications.aitea_fe.MODE_ID";

    /** 模擬試験結果にて回答した問題の回答内容を表示するための、回答内容リストを送受信する際に用いる */
    public final static String SELECT_ANSWER_LIST = "com.support.qualifications.aitea_fe.SELECT_ANSWER_LIST";

    /** 画面上部に表示するToolberに表示するテキストを送受信する際に用いる */
    public final static String TOOLBER_TITLE = "com.support.qualifications.aitea_fe.TOOLBER_TITLE";

    /** ユーザの回答を送受信する際に用いる */
    public final static String USER_ANSWER = "com.support.qualifications.aitea_fe.USER_ANSWER";

    /** 問題情報を送受信する際に用いる */
    public final static String CURRENT_Q_ID = "com.support.qualifications.aitea_fe.CURRENT_Q_ID";

}
