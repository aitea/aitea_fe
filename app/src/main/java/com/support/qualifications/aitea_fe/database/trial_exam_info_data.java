package com.support.qualifications.aitea_fe.database;

import java.io.Serializable;


public class trial_exam_info_data implements Serializable{

	/** 出題ID( 1～80 ) */
	private String set_q_id;

	/** 問題ID( Q0001～Q9999 ) */
	private String q_id;

	/** ユーザの回答( 'ア' ) */
	private String user_answer;

	/** 正誤( 1:正解,0:不正解 ) */
	private int correct_mistake;

	/** 回答日時( yyyy-mm-dd hh:mm:ss ) */
	private String user_answer_date;

	/** 回答画面を表示してから、回答するまでの経過時間( 秒 ) */
	private int elapsed_time;

	/**
	 * 出題IDを取得する
	 * @return 出題ID( 1～80 )
	 */
	public String getSet_q_id() {
		return set_q_id;
	}

	/**
	 * 出題IDを格納する
	 * @param set_q_id 出題ID( 1～80 )
	 */
	public void setSet_q_id(String set_q_id) {

		this.set_q_id = set_q_id;
	}

	/**
	 * 問題IDを取得する
	 * @return 問題ID( Q0001～Q9999 )
	 */
	public String getQ_id() {
		return q_id;
	}

	/**
	 * 問題IDを格納する
	 * @param q_id 問題ID( Q0001～Q9999 )
	 */
	public void setQ_id(String q_id) {
		this.q_id = q_id;
	}

	/**
	 * 回答を取得する
	 * @return 回答( 'ア' )
	 */
	public String getUser_answer() {
		return user_answer;
	}

	/**
	 * 回答を格納する
	 * @param user_answer 回答( 'ア' )
	 */
	public void setUser_answer(String user_answer) {
		this.user_answer = user_answer;
	}

	/**
	 * 正誤を取得する
	 * @return 正誤 ( 1:正解,0:不正解 )
	 */
	public int getCorrect_mistake() {
		return correct_mistake;
	}

	/**
	 * 正誤を格納する
	 * @param correct_mistake 正誤 ( 1:正解,0:不正解 )
	 */
	public void setCorrect_mistake(int correct_mistake) {
		this.correct_mistake = correct_mistake;
	}

	/**
	 * 回答日時を取得する
	 * @return 回答日時( yyyy-mm-dd hh:mm:ss )
	 */
	public String getUser_answer_date() {
		return user_answer_date;
	}

	/**
	 * 回答日時を格納する
	 * @param user_answer_date 回答日時( yyyy-mm-dd hh:mm:ss )
	 */
	public void setUser_answer_date(String user_answer_date) {
		this.user_answer_date = user_answer_date;
	}

	/**
	 * 経過時間を取得する
	 * @return 経過時間( 秒 )
	 */
	public int getElapsed_time() {
		return elapsed_time;
	}

	/**
	 * 経過時間を格納する
	 * @param elapsed_time 経過時間( 秒 )
	 */
	public void setElapsed_time(int elapsed_time) {
		this.elapsed_time = elapsed_time;
	}

}
