package com.support.qualifications.aitea_fe.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.support.qualifications.aitea_fe.R;

/**
 * 「このアプリについて」の画面
 */
public class Activity_About extends Activity_Base {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abount);

        //バージョン情報セット
        ((TextView)findViewById(R.id.versionText)).setText(getVertionName());

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_about));
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * バージョン情報を取得
     * @return バージョン名
     */
    private String getVertionName(){
        PackageManager pm = this.getPackageManager();
        String versionName = "";
        try{
            PackageInfo packageInfo = pm.getPackageInfo(this.getPackageName(), 0);
            versionName = packageInfo.versionName;
        }catch(PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }
        return versionName;
    }

}
