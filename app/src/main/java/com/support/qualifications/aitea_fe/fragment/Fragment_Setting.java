package com.support.qualifications.aitea_fe.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.support.qualifications.aitea_fe.activity.Activity_About;
import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;

/**
 * 設定項目を表示するフラグメント
 */
public class Fragment_Setting extends PreferenceFragment {


    public Fragment_Setting() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //プリファレンスをセット
        addPreferencesFromResource(R.xml.pref_setting);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ////作成したfragmentの初期化処理はここに書く///

        //クリックリスナーを設定する
        setOnClickLitener();

    }

    /**
     * 各設定項目のクリックリスナーを登録する
     */
    private void setOnClickLitener() {

        //サーバへの送信許可クリック
        findPreference(getString(R.string.key_permissionSendToServer)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForPermissonSendToServer();
                return true;
            }
        });

        //克服機能についてをクリック
        findPreference(getString(R.string.key_aboutOvercome)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForAboutOvercome();
                return true;
            }
        });

//        //公式サイトを見る
//        findPreference(getString(R.string.key_officialSite)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            public boolean onPreferenceClick(Preference preference) {
//                return true;
//            }
//        });

        //回答履歴をリセットする
        findPreference(getString(R.string.key_resetUserAnswerArchive)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForResetUserAnswerArchive();
                return true;
            }
        });

        //模擬試験の成績情報をリセットする
        findPreference(getString(R.string.key_resetTrialExamPerformance)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForResetTrialExamPerformance();
                return true;
            }
        });

        //お気に入り問題をリセットする
        findPreference(getString(R.string.key_resetFavoriteFlag)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForResetFavoriteFlag();
                return true;
            }
        });

        //克服問題ををリセットする
        findPreference(getString(R.string.key_resetOvercomeFlag)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForResetOvercomeFlag();
                return true;
            }
        });

        //全データををリセットする
        findPreference(getString(R.string.key_resetAllData)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                //アラート表示
                showAlertForResetAllData();
                return true;
            }
        });

        //このアプリについて
        findPreference(getString(R.string.key_aboutThisApplication)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(),Activity_About.class);
                startActivity(intent);
                return true;
            }
        });

        //利用上の注意
        findPreference(getString(R.string.key_notesOnUse)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                showAlertFornotesOnUse();
                return true;
            }
        });

        //個人情報の取扱いについて
        findPreference(getString(R.string.key_handlingOfPersonalInformation)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                showAlertForhandlingOfPersonalInformation();
                return true;
            }
        });

    }



    /**
     * サーバへの送信許可の説明アラート
     */
    private void showAlertForPermissonSendToServer(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_server_send_permission_explain));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_server_send_permission_explain));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.show();
    }

    /**
     * 克服機能についての説明
     */
    private void showAlertForAboutOvercome(){
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_overcome_explain));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_overcome_explain));

        alertDialogBuilder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    /**
     * 回答履歴をリセットするときのアラートを表示する
     */
    private void showAlertForResetUserAnswerArchive(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_answer_archive_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_answer_archive_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //回答履歴をリセット
                resetUserAnswerArchive();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 回答履歴削除で行う処理
     */
    private void resetUserAnswerArchive(){

        //問題情報テーブルの間違い回数をすべて0に
        reset_mistake_count();

        //ジャンルの正答率をリセット
        reset_each_class_correct_rate();

        //回答履歴テーブルのレコード削除
        delete_tbl_info(DB_ColumnNames.ANSWER_ARCHIVE.TABLE_NAME);
    }

    /**
     * 模擬試験の成績情報をリセットするときのアラートを表示する
     */
    private void showAlertForResetTrialExamPerformance(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_trial_exam_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_trial_exam_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //模擬試験の途中保存のデータは消す必要がないので消さない

                //成績情報をリセット
                resetTrialExamPerformance();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 成績情報削除で行う処理
     */
    private void resetTrialExamPerformance(){

        //成績情報テーブルレコード削除
        delete_tbl_info(DB_ColumnNames.PERFORMANCE_INFO.TABLE_NAME);
    }
    /**
     * お気に入り問題をリセットするときのアラートを表示する
     */
    private void showAlertForResetFavoriteFlag(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_favorite_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_favorite_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //お気に入りフラグすべておろす
                downAllFavoriteFlag();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 克服問題をリセットするときのアラートを表示する
     */
    private void showAlertForResetOvercomeFlag(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_overcome_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_overcome_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //克服フラグすべておろす
                downAllOvercomeFlag();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 全データをリセットするときのアラートを表示する
     */
    private void showAlertForResetAllData(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.alert_title_all_data_reset));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_all_data_reset));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //回答履歴リセット
                resetUserAnswerArchive();
                //模擬試験成績情報リセット
                resetTrialExamPerformance();
                //お気に入りフラグすべておろす
                downAllFavoriteFlag();
                //克服フラグすべておろす
                downAllOvercomeFlag();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 利用上の注意
     */
    private void showAlertFornotesOnUse(){
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.title_notesOnUse));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_notesOnUse));

        alertDialogBuilder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 個人情報の取扱い
     */
    private void showAlertForhandlingOfPersonalInformation(){
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getString(R.string.title_handlingOfPersonalInformation));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_handlingOfPersonalInformation));

        alertDialogBuilder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 全てのお気に入りフラグをおろす
     */
    private void downAllFavoriteFlag(){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.down_all_favorite_flag());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    /**
     * 全ての克服フラグをおろす
     */
    private void downAllOvercomeFlag(){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.down_all_overcome_flag());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    /**
     * 間違い回数を全て0にする
     */
    private void reset_mistake_count(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.reset_mistake_count());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
    /**
     * 全ジャンルの正答率情報をリセットする
     */
    private void reset_each_class_correct_rate(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();
        db.execSQL(DB_SqlMaker.TBL_GENRE_INFO.reset_each_class_correct_rate());
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
    /**
     * 指定テーブルの全レコードを削除する
     * @param table_name 指定テーブル名
     */
    protected void delete_tbl_info(String table_name){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.delete(table_name, null, null);
        db.close();
    }

}
