package com.support.qualifications.aitea_fe.processing;

/**
 * 各ジャンルの出題確率を演算するためのもの
 */
public class q_rate_info {

    /** 分野コード( 1～* ) */
    private int field_code;

    /** 大分類コード( 1～** ) */
    private int l_class_code;

    /** 中分類コード( 1～** ) */
    private int m_class_code;

    /** 小分類コード( 1～** ) */
    private int s_class_code;

    /** 正答率の昇順の順位( 1～** ) */
    private int rank;

    /** 重複順位数:同じ順位をもつジャンルの個数 */
    private int rank_repetedCount;

    /** 正答率 ( 0.*** ～1.*** )*/
    private double correct_rate;

    /** 出題確率( 0.*** ) */
    private double set_q_rate;

    /**
     * 最初は全ジャンル1位にする
     * 重複順位はインクリメントするため初期値を1とする
     */
    public q_rate_info(){
        this.rank = 1;
        this.rank_repetedCount = 1;
    }

    /**
     * 分野コードを取得
     * @return 分野コード( 1～* )
     */
    public int getField_code() {
        return field_code;
    }

    /**
     * 分野コードを格納
     * @param field_code 分野コード( 1～* )
     */
    public void setField_code(int field_code) {
        this.field_code = field_code;
    }

    /**
     * 大分類コードを取得
     * @return 大分類コード( 1～** )
     */
    public int getL_class_code() {
        return l_class_code;
    }

    /**
     * 大分類コードを格納
     * @param l_class_code 大分類コード( 1～** )
     */
    public void setL_class_code(int l_class_code) {
        this.l_class_code = l_class_code;
    }

    /**
     * 中分類コードを取得
     * @return 中分類コード( 1～** )
     */
    public int getM_class_code() {
        return m_class_code;
    }

    /**
     * 中分類コードを格納
     * @param m_class_code 中分類コード( 1～** )
     */
    public void setM_class_code(int m_class_code) {
        this.m_class_code = m_class_code;
    }

    /**
     * 小分類コードを取得
     * @return 小分類コード( 1～** )
     */
    public int getS_class_code() {
        return s_class_code;
    }

    /**
     * 小分類コードを格納
     * @param s_class_code 小分類コード( 1～** )
     */
    public void setS_class_code(int s_class_code) {
        this.s_class_code = s_class_code;
    }

    /**
     * 順位を取得
     * @return 順位( 1～** )
     */
    public int getRank() {
        return rank;
    }

    /**
     * 順位を格納
     * @param rank 順位( 1～** )
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * 重複順位数を取得
     * @return 重複順位数
     */
    public int getRank_repetedCount() {
        return rank_repetedCount;
    }

    /**
     * 重複順位数を格納
     * @param rank_repetedCount 重複順位数
     */
    public void setRank_repetedCount(int rank_repetedCount) {
        this.rank_repetedCount = rank_repetedCount;
    }

    /**
     * 正答率を取得
     * @return 正答率( 0.*** ～1.*** )
     */
    public double getCorrect_rate() {
        return correct_rate;
    }

    /**
     * 正答率を格納
     * @param correct_rate 正答率( 0.*** ～1.*** )
     */
    public void setCorrect_rate(double correct_rate) {
        this.correct_rate = correct_rate;
    }

    /**
     * 出題確率を取得
     * @return 出題確率( 0.*** )
     */
    public double getSet_q_rate() {
        return set_q_rate;
    }

    /**
     * 出題確率を格納
     * @param set_q_rate 出題確率( 0.*** )
     */
    public void setSet_q_rate(double set_q_rate) {
        this.set_q_rate = set_q_rate;
    }
}
