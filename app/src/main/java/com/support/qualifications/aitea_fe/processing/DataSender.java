package com.support.qualifications.aitea_fe.processing;

import android.net.Uri;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.List;

/**
 * サーバに非同期通信でデータを送信するクラス
 *
 */
public class DataSender extends AsyncTask<Uri.Builder, Void, String> {

    private List<NameValuePair> list;

    private static final String SERVERADRESS = "http://aiteaz.com/Aitea-Manager-Inter/xaxicxzpgz"; //送信先サーバIPアドレス

    public DataSender(List<NameValuePair> list) {
        this.list = list; //送信するデータ
    }

    /**
     *  AsyncTaskのexecuteメソッド呼出時に非同期で処理されるメソッド
     *
     */
    @Override
    protected String doInBackground(Uri.Builder... builder) {

        HttpPost post = new HttpPost(SERVERADRESS); //送信先の指定
        HttpClient client = new DefaultHttpClient();

        try {
            /* 送信するデータのセット、HttpPostの実行 */
            post.setEntity(new UrlEncodedFormEntity(list,"UTF-8"));
            client.execute(post);
            return "送信成功" ;

        } catch (Exception e) {
            e.printStackTrace();
            return "送信失敗" ;
        }
    }
}
