// 参考URL
// http://techbooster.jpn.org/andriod/ui/986/

package com.support.qualifications.aitea_fe.activity;

import android.os.Bundle;

import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.fragment.Fragment_Setting;

/**
 * 設定画面
 */
public class Activity_OptionMenu_Setting extends Activity_Base {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oputionmenu_setting);

        //fragmentをセット
        getFragmentManager().beginTransaction()
                .replace(R.id.setting_content, new Fragment_Setting())
                .commit();

        //ナビゲーションドロワーをセット
        setNavigation((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_optionmenu_setting));
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }


}