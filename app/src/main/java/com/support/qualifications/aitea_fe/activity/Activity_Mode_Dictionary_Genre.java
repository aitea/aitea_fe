package com.support.qualifications.aitea_fe.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.genre_info_data;

import java.util.ArrayList;
import java.util.List;

/**
 * ジャンル別モードの画面
 */
public class Activity_Mode_Dictionary_Genre extends Activity_Base {

    /** スピナーの先頭に表示するアイテム */
    private static final String NULL_ITEM = "指定しない";

    /** モードID */
    private int mode_id;

    /** 分野スピナー */
    private Spinner spinner_filed;

    /** 大分類スピナー */
    private Spinner spinner_class_large;

    /** 中分類スピナー */
    private Spinner spinner_class_middle;

    /** 小分類スピナー */
    private Spinner spinner_class_small;

    /** 分野情報 */
    private genre_info_data[] field_i_data;

    /** 大分類情報 */
    private genre_info_data[] lclass_i_data;

    /** 中分類情報 */
    private genre_info_data[] mclass_i_data;

    /** 小分類情報 */
    private genre_info_data[] sclass_i_data;

    /** 選択した小分類リストの位置 */
    private int select_sclass_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_dictionary_genre);

        getIntentValue();

        //ナビゲーションドロワーセット
        setNavigation((android.support.v7.widget.Toolbar)findViewById(R.id.toolbar_mode_dictionary_genre));

        //全てのスピナーをxmlから取得
        setSpinnerViews();

        //分野スピナーセット
        setSpinner_filed();

        //大分類スピナーセット
        setSpinner_L_class();

        //中分類スピナーセット
        setSpinner_M_class();

        //小分類スピナーセット
        setSpinner_S_class();

        //検索ボタンのクリックリスナーセット
        setButtonClickListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * 全てのスピナーをxmlから取得
     */
    private void setSpinnerViews(){
        spinner_filed = (Spinner)findViewById(R.id.spinner_filed);
        spinner_class_large = (Spinner)findViewById(R.id.spinner_class_large);
        spinner_class_middle = (Spinner)findViewById(R.id.spinner_class_middle);
        spinner_class_small = (Spinner)findViewById(R.id.spinner_class_small);
    }

    /**
     * インテントからデータを取得
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
    }

    /**
     * 分野スピナーにデータセット
     */
    private void setData_Spinner_field(){
        field_i_data = get_field_info();
        List<String> listItem_field = mkField_name_list(field_i_data);
        ArrayAdapter<String> spinAdapter_field = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem_field);
        spinner_filed.setAdapter(spinAdapter_field);
    }

    /**
     * 分野スピナーセット
     */
    private void setSpinner_filed(){

        //分野スピナーにデータセット
        setData_Spinner_field();

       spinner_filed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               //大分類にデータセット
               setData_Spinner_L_class(position);

               //中分類非表示
               setNullMClassSpinner();

               //小分類非表示
               setNullSClassSpinner();
           }

           @Override
           public void onNothingSelected(AdapterView<?> arg0) {
           }
       });
    }
    /**
     * 大分類スピナーにデータセット
     * @param position 選択したリストの位置
     */
    private void setData_Spinner_L_class(int position)
    {
        lclass_i_data = get_Lclass_info(field_i_data[position].getField_code());
        List<String> listItem = mkL_class_name_list(lclass_i_data);
        ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
        spinner_class_large.setAdapter(spinAdapter_class);
    }

    /**
     * 大分類スピナーの設定
     */
    private void setSpinner_L_class(){

        //先頭の分野のデータをセット
        setData_Spinner_L_class(0);

        spinner_class_large.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //小分類を非表示
                setNullSClassSpinner();

                if (position == 0) {
                    //中分類を非表示
                    setNullMClassSpinner();
                } else {
                    //中分類にデータセット
                    setData_Spinner_M_class(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * 中分類スピナーにデータセット
     * @param position 選択したリストの位置
     */
    private void setData_Spinner_M_class(int position)
    {
        mclass_i_data = get_Mclass_info(
                lclass_i_data[position - 1].getField_code(),
                lclass_i_data[position - 1].getL_class_code()
        );
        List<String> listItem = mkM_class_name_list(mclass_i_data);
        ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
        spinner_class_middle.setAdapter(spinAdapter_class);
        spinner_class_middle.setEnabled(true);
    }

    /**
     * 中分類スピナーの設定
     */
    private void setSpinner_M_class(){

        //初期は非表示
        setNullMClassSpinner();

        spinner_class_middle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    //小分類非表示
                    setNullSClassSpinner();
                } else {
                    //小分類にデータセット
                    setData_Spinner_S_class(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * 小分類スピナーにデータセット
     * @param position 選択したリストの位置
     */
    private void setData_Spinner_S_class(int position)
    {
        sclass_i_data = get_Sclass_info(
                mclass_i_data[position - 1].getField_code(),
                mclass_i_data[position - 1].getL_class_code(),
                mclass_i_data[position - 1].getM_class_code()
        );
        List<String> listItem = mkS_class_name_list(sclass_i_data);
        ArrayAdapter<String> spinAdapter_class = new ArrayAdapter<String>(Activity_Mode_Dictionary_Genre.this, android.R.layout.simple_list_item_1, listItem);
        spinner_class_small.setAdapter(spinAdapter_class);
        spinner_class_small.setEnabled(true);
    }

    /**
     * 小分類スピナーの設定
     */
    private void setSpinner_S_class(){

        //初期は非表示
        setNullSClassSpinner();

        spinner_class_small.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //選択したリストの位置を保持
                select_sclass_id = position - 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    /**
     * ボタンのクリックリスナーをセットする
     */
    private void setButtonClickListener(){

        // 検索ボタン
        findViewById(R.id.button_mode_dictionary_genre_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //問題一覧画面に遷移する
                goQuestion_List();
            }
        });
    }

    /**
     * 問題一覧画面に遷移する
     */
    private void goQuestion_List(){

        //問題IDリスト
        String[] out_q_id_data = null;

        //ジャンル名
        String genre_name = null;

        //小分類まで選択している
        if (select_sclass_id != -1) {
            out_q_id_data = getOut_q_i_data(sclass_i_data[0].getField_code(), sclass_i_data[0].getL_class_code(), sclass_i_data[0].getM_class_code(), sclass_i_data[select_sclass_id].getS_class_code());
            genre_name = get_genre_name(sclass_i_data[0].getField_code(), sclass_i_data[0].getL_class_code(), sclass_i_data[0].getM_class_code(), sclass_i_data[select_sclass_id].getS_class_code());
        }
        //中分類まで選択している
        else if (sclass_i_data != null) {
            out_q_id_data = getOut_q_i_data(sclass_i_data[0].getField_code(), sclass_i_data[0].getL_class_code(), sclass_i_data[0].getM_class_code());
            genre_name = get_genre_name(sclass_i_data[0].getField_code(), sclass_i_data[0].getL_class_code(), sclass_i_data[0].getM_class_code());
        }
        //大分類まで選択している
        else if (mclass_i_data != null) {
            out_q_id_data = getOut_q_i_data(mclass_i_data[0].getField_code(), mclass_i_data[0].getL_class_code());
            genre_name = get_genre_name(mclass_i_data[0].getField_code(), mclass_i_data[0].getL_class_code());
        }
        //分野まで選択している
        else {
            out_q_id_data = getOut_q_i_data(lclass_i_data[0].getField_code());
            genre_name = get_genre_name(lclass_i_data[0].getField_code());
        }

        Intent intent = new Intent(Activity_Mode_Dictionary_Genre.this, Activity_Mode_Dictionary_Question_List.class);
        intent.putExtra(IntentKeyword.Q_ID_LIST, out_q_id_data);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);
        intent.putExtra(IntentKeyword.TOOLBER_TITLE, genre_name);
        startActivity(intent);
    }

    /**
     * 中分類のスピナーを非表示にする
     */
    private void setNullMClassSpinner(){
        mclass_i_data = null;
        spinner_class_middle.setAdapter(getNull_adapter());
        spinner_class_middle.setEnabled(false);
    }

    /**
     * 小分類のスピナーを非表示にする
     */
    private void setNullSClassSpinner(){
        sclass_i_data = null;
        spinner_class_small.setAdapter(getNull_adapter());
        spinner_class_small.setEnabled(false);
    }

    /**
     * スピナーを非表示にするための空のアダプタを取得
     * @return 空のアダプタ
     */
    private ArrayAdapter<String> getNull_adapter(){
        List<String> listNull = new ArrayList<String>();
        listNull.add(NULL_ITEM);
        return new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listNull);
    }


    /**
     * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する分野名と大分類名と中分類名と小分類名を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @param m_class_code	中分類コード
     * @param s_class_code	小分類コード
     * @return 分野名と大分類名と中分類名と小分類名
     */
    private String get_genre_name(int field_code,int l_class_code,int m_class_code,int s_class_code){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_genre_name(field_code, l_class_code, m_class_code, s_class_code), null);
        if (c.moveToNext()) {
            String genre_name =
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_NAME)) + "＞" +
                            c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_NAME));
            return genre_name;
        };
        c.close();
        db.close();
        return null;
    }

    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する分野名と大分類名と中分類名を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @param m_class_code	中分類コード
     * @return 分野名と大分類名と中分類名
     */
    private String get_genre_name(int field_code,int l_class_code,int m_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_genre_name(field_code, l_class_code, m_class_code), null);
        if (c.moveToNext()) {
            String genre_name =
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)) + "＞" +
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME)) + "＞" +
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_NAME));

            return genre_name;
        };

        c.close();
        db.close();

        return null;
    }

    /**
     * 引数で指定した分野コードと大分類コードに属する分野名と大分類名を取得する
     * @param field_code	分野コード
     * @param l_class_code	大分類コード
     * @return 分野名と大分類名
     */
    private String get_genre_name(int field_code,int l_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_genre_name(field_code, l_class_code), null);
        if (c.moveToNext()) {
            String genre_name =
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)) + "＞" +
                    c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME));

            return genre_name;
        };
        c.close();
        db.close();
        return null;
    }

    /**
     * 引数で指定した分野コードに属する分野名を取得する
     * @param field_code 分野コード
     * @return 分野名
     */
    private String get_genre_name(int field_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_genre_name(field_code), null);
        if (c.moveToNext()) {
            String genre_name = c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME));
            return genre_name;
        };
        c.close();
        db.close();
        return null;
    }

    /**
     * 全ての分野コード、分野名を取得する
     * @return 分野コード、分野名をもつジャンル情報クラス
     */
    private genre_info_data[] get_field_info(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_all_field_info(), null);

        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_CODE)));
            out_g_i_data[i].setField_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.FIELD_NAME)));
            i++;
        }
        c.close();
        db.close();
        return out_g_i_data;
    }

    /**
     * 引数で指定した分野コードに属する大分類コード、大分類名を取得するsql文を返す
     * @param field_code 分野コード
     * @return 大分類コード、大分類名をもつジャンル情報クラス
     */
    private genre_info_data[] get_Lclass_info(int field_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_l_class_info(field_code), null);
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_CODE)));
            out_g_i_data[i].setL_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.L_CLASS_NAME)));
            i++;
        }
        c.close();
        db.close();
        return out_g_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードに属する中分類コード、中分類名を取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @return 中分類コード、中分類名をもつジャンル情報クラス
     */
    private genre_info_data[] get_Mclass_info(int field_code,int l_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_m_class_info(field_code, l_class_code), null);
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()){
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(l_class_code);
            out_g_i_data[i].setM_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_CODE)));
            out_g_i_data[i].setM_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.M_CLASS_NAME)));
            i++;
        }
        c.close();
        db.close();

        return out_g_i_data;
    }

    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する小分類コード、小分類名を取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @return 小分類コード、小分類名を持つジャンル情報クラス
     */
    private genre_info_data[] get_Sclass_info(int field_code,int l_class_code,int m_class_code){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_GENRE_INFO.get_s_class_info(field_code, l_class_code, m_class_code), null);
        genre_info_data[] out_g_i_data = new genre_info_data[c.getCount()];

        int i = 0;
        while (c.moveToNext()) {
            out_g_i_data[i] = new genre_info_data();
            out_g_i_data[i].setField_code(field_code);
            out_g_i_data[i].setL_class_code(l_class_code);
            out_g_i_data[i].setM_class_code(m_class_code);
            out_g_i_data[i].setS_class_code(c.getInt(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_CODE)));
            out_g_i_data[i].setS_class_name(c.getString(c.getColumnIndex(DB_ColumnNames.GENRE_INFO.S_CLASS_NAME)));
            i++;
        };
        c.close();
        db.close();
        return out_g_i_data;
    }


    /**
     * 引数で指定した分野コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @return　問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code), null);

        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code), null);

        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];

        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();

        return out_q_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードと中分類コードに属する問題IDリストを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code, m_class_code), null);
        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }


    /**
     * 引数で指定した分野コードと大分類コードと中分類コードと小分類コードに属する問題IDリストをを年度、回次で降順、問題IDの昇順で取得する
     * @param field_code 分野コード
     * @param l_class_code 大分類コード
     * @param m_class_code 中分類コード
     * @param s_class_code 小分類コード
     * @return 問題IDリスト
     */
    private String[] getOut_q_i_data(int field_code, int l_class_code , int m_class_code , int s_class_code) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_q_id(field_code, l_class_code, m_class_code, s_class_code), null);
        if(c.getCount() == 0){
            return null;
        }
        String[] out_q_i_data = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()){
            out_q_i_data[i] = new String();
            out_q_i_data[i] = c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_ID));
            i++;
        }
        c.close();
        db.close();
        return out_q_i_data;
    }

    /**
     * 分野名リストを生成
     * @param g_i_data リストに追加する分野名をもつジャンルデータ配列
     * @return 分野名リスト
     */
    private List<String> mkField_name_list(genre_info_data[] g_i_data) {
        List<String> field_name_list = new ArrayList<String>();
        for (int i = 0; i < g_i_data.length; i++) {
            field_name_list.add(g_i_data[i].getField_name());
        }
        return field_name_list;
    }


    /**
     *  大分類名リストを生成
     * @param g_i_data  リストに追加する大分類名をもつジャンルデータ配列
     * @return 大分類名リスト
     */
    private List<String> mkL_class_name_list(genre_info_data[] g_i_data) {
        List<String> l_class_name_list = new ArrayList<String>();
        l_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            l_class_name_list.add(g_i_data[i].getL_class_name());
        }
        return l_class_name_list;
    }

    /**
     *  中分類名リストを生成
     * @param g_i_data  リストに追加する中分類名をもつジャンルデータ配列
     * @return 中分類名リスト
     */
    private List<String> mkM_class_name_list(genre_info_data[] g_i_data) {
        List<String> m_class_name_list = new ArrayList<String>();
        m_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            m_class_name_list.add(g_i_data[i].getM_class_name());
        }
        return m_class_name_list;
    }

    /**
     *  小分類名リストを生成
     * @param g_i_data  リストに追加する小分類名をもつジャンルデータ配列
     * @return 小分類名リスト
     */
    private List<String> mkS_class_name_list(genre_info_data[] g_i_data) {
        List<String> s_class_name_list = new ArrayList<String>();
        s_class_name_list.add(NULL_ITEM);
        for (int i = 0; i < g_i_data.length; i++) {
            s_class_name_list.add(g_i_data[i].getS_class_name());
        }
        return s_class_name_list;
    }
}