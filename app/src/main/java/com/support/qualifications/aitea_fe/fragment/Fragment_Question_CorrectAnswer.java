package com.support.qualifications.aitea_fe.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.AiteaCommonData;
import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.question_info_data;

/**
 * 問題の解説検索用Webページを表示するためのFragment
 */
public class Fragment_Question_CorrectAnswer extends Fragment {

    /** 作成するView */
    private View correctAnswerView;

    /** Webページ表示用のView */
    private WebView myWebView;

    public Fragment_Question_CorrectAnswer(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        correctAnswerView = inflater.inflate(R.layout.fragment_question_correctanswer, container, false);

        String q_id = getArguments().getString(IntentKeyword.CURRENT_Q_ID);

        question_info_data questionInfoData = get_year_turn_qno(q_id);

        setWebview(questionInfoData);

        return correctAnswerView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * webViewを設定する
     */
    private void setWebview(question_info_data q_i_data){

        myWebView = (WebView) correctAnswerView.findViewById(R.id.webview);

        //javascript有効にする
        myWebView.getSettings().setJavaScriptEnabled(true);

        //これがないと、新しくウインドウが開かれてしまう
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        //urlをロードする
        myWebView.loadUrl(geturl(q_i_data.getYear(), q_i_data.getTurn(), q_i_data.getQ_no()));
    }

    /**
     * WebViewに表示するURLを取得する
     * @param year 年度( 平成27年度 )
     * @param turn 回次( 春期 )
     * @param q_no 問題No( 1 ～ ** )
     * @return 作成したURL
     */
    private String geturl(String year, String turn, int q_no) {
        return "https://www.google.co.jp/search?q=" + AiteaCommonData.MY_EXAM_CATEGORY_NAME + " " + year + " " + turn + " " + "問" + q_no;
    }

    /**
     * Webページを前のページに戻す
     * もし、履歴がなければ戻らない
     * @return 戻った:true , 戻らなかった:false
     */
    public boolean goBackMyWebView(){
        if(myWebView.canGoBack()){
            myWebView.goBack();
            return true;
        }else{
            return false;
        }
    }

    /**
     * 引数で指定した問題IDに該当する問題回答画面を表示するのに必要なデータを取得する
     * @param q_id 問題ID
     * @return 問題回答画面を表示するのに必要なデータ
     */
    private question_info_data get_year_turn_qno(String q_id) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(getActivity());
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();

        question_info_data out_q_i_data;

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_year_turn_qno(q_id), null);
        if(c.moveToNext()){
            out_q_i_data = new question_info_data();
            out_q_i_data.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            out_q_i_data.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            out_q_i_data.setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
        }else{
            return null;
        }
        c.close();

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        return out_q_i_data;
    }

}
