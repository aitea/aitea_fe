package com.support.qualifications.aitea_fe.processing;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * グラフ要素をフォーマットするクラス
 */
public class myLineFormatter implements ValueFormatter {

    private DecimalFormat mFormat;

    public myLineFormatter() {
        //表示フォーマットを設定
        mFormat = new DecimalFormat("      ##0"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return mFormat.format(value); // e.g. append a dollar-sign
    }
}
