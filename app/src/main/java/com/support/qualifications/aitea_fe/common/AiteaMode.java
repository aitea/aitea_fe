package com.support.qualifications.aitea_fe.common;

/**
 * 各モードを識別するためのモードID
 */
public final class AiteaMode {

    /** Aiteaモード */
    public static final int AITEA = 1;

    /** 模擬試験モード */
    public static final int TRIALEXAM = 2;

    /** 年度別モード */
    public static final int YEAR = 3;

    /** ジャンル別モード */
    public static final int GENRE = 4;

    /** 復習モード */
    public static final int REVIEW = 5;

    /** お気に入りモード */
    public static final int FAVORITE = 6;

    /** 克服一覧 */
    public static final int OVERCOME = 7;
}