package com.support.qualifications.aitea_fe.processing;

/**
 * ActivityのonRestart時にActivityをfinish()するための判定フラグ
 */
public class Finishflag {

    /** finish()を走らせるかどうかのフラグ */
    private boolean flag;

    /**
     * フラグを取得する
     * @return true : finish()を走らせる, false : finish()を走らせない
     */
    public boolean getFlag() {
        return flag;
    }

    /**
     * フラグをセットする
     * @param flag true : finish()を走らせる, false : finish()を走らせない
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}