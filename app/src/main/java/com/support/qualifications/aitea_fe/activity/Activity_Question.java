package com.support.qualifications.aitea_fe.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.support.qualifications.aitea_fe.R;
import com.support.qualifications.aitea_fe.common.AiteaMode;
import com.support.qualifications.aitea_fe.common.Date;
import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.database.AiteaOpenHelper;
import com.support.qualifications.aitea_fe.database.DB_ColumnNames;
import com.support.qualifications.aitea_fe.database.DB_SqlMaker;
import com.support.qualifications.aitea_fe.database.question_info_data;
import com.support.qualifications.aitea_fe.processing.Question_PagerAdapter;
import com.support.qualifications.aitea_fe.tester.CustomUncaughtExceptionHandler;

/**
 * 問題回答画面
 */
public class Activity_Question extends Activity_Base {

    /** モードID */
    private int mode_id;

    /** 問題IDリスト */
    private String[] q_id_data;

    /** 問題IDリスト内の現在の添え字( 0 ～ q_id_data.length - 1 ) */
    private int currentId;

    /** 問題回答開始時間 */
    private long startTime;

    /** ユーザの回答 */
    private String selectAnswer;

    /** 問題リストを表示するViewPager */
    private ViewPager myPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        // UncaughtExceptionHandlerを実装したクラスをセットする。
        CustomUncaughtExceptionHandler customUncaughtExceptionHandler = new CustomUncaughtExceptionHandler(
                getApplicationContext());
        Thread.setDefaultUncaughtExceptionHandler(customUncaughtExceptionHandler);

        //Intentからデータを受け取る
        getIntentValue();

        //ViewPager設定
        setViewPager();

        //ツールバーのセット
        setToolbar();

        //スワイプ時の処理
        setStateForSwipe();

        //回答ボタンのクリックリスナーを設定
        setAnswerButtonsClickListener();
    }
    @Override
    protected void onPause() {
        super.onPause();
        closeNavigation();
    }

    /**
     * スワイプ時の処理
     */
    private void setStateForSwipe() {
        setToolbarText();
        setFlagIcon(q_id_data[currentId]);
        if (mode_id == AiteaMode.TRIALEXAM) {
            //前回の回答に一致する選択肢のボタンの文字色を変える。
            setPastAnswerButton();
            //問題解き始めの時間を保持
            startTime = SystemClock.elapsedRealtime();
        }
    }

    /**
     * Intentで渡されたデータを取得する
     */
    private void getIntentValue(){
        Intent intent = getIntent();
        currentId = intent.getIntExtra(IntentKeyword.SELECT_Q_ID, 0);
        q_id_data = intent.getStringArrayExtra(IntentKeyword.Q_ID_LIST);
        mode_id = intent.getIntExtra(IntentKeyword.MODE_ID, 0);
    }

    /**
     * 戻るボタンを押下した際の処理
     * @return Return <code>true</code> to prevent this event from being propagated
     * further, or <code>false</code> to indicate that you have not handled
     * this event and it should continue to be propagated.
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        // Keyボタン(BACKボタン)押下時の処理
        if(keyCode == KeyEvent.KEYCODE_BACK){
            switch (mode_id){
                case AiteaMode.GENRE:
                case AiteaMode.YEAR:
                    Activity_Mode_Dictionary_Question_List.scrollValue.setScrollposition(currentId);
                case AiteaMode.TRIALEXAM:
                    Activity_Mode_TrialExam_List.scrollValue.setScrollposition(currentId);
                case AiteaMode.REVIEW:
                    Activity_Mode_Review.scrollValue.setScrollposition(currentId);
                case AiteaMode.FAVORITE:
                    Activity_Mode_Favorite.scrollValue.setScrollposition(currentId);
                case AiteaMode.OVERCOME:
                    Activity_OptionMenu_Overcome_List.scrollValue.setScrollposition(currentId);
                default:
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * 前回の回答内容が分かるようにするため、前回の回答に一致する選択肢の
     * ボタンの文字色を変える。
     */
    private void setPastAnswerButton(){

        String get_past_answer = get_trial_exam_user_answer(currentId + 1);

        //文字色初期化
        initPastAnswerButton();

        int setColor = getResources().getColor(R.color.Red);
        if(get_past_answer != null){
            if (get_past_answer.equals(getString(R.string.question_answer_A))) {
                ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerA)).setTextColor(setColor);
            } else if (get_past_answer.equals(getString(R.string.question_answer_I))) {
                ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerI)).setTextColor(setColor);
            } else if (get_past_answer.equals(getString(R.string.question_answer_U))) {
                ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerU)).setTextColor(setColor);
            } else if (get_past_answer.equals(getString(R.string.question_answer_E))) {
                ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerE)).setTextColor(setColor);
            } else if (get_past_answer.equals(getString(R.string.question_answer_Q))) {
                ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerQ)).setTextColor(setColor);
            }
        }
    }

    /**
     * ボタンの文字色を初期化する
     */
    private void initPastAnswerButton(){
        int setColor = getResources().getColor(R.color.white);
        ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerA)).setTextColor(setColor);
        ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerI)).setTextColor(setColor);
        ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerU)).setTextColor(setColor);
        ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerE)).setTextColor(setColor);
        ((com.beardedhen.androidbootstrap.BootstrapButton)findViewById(R.id.Q_answerQ)).setTextColor(setColor);
    }

    /**
     * 問題リスト(スワイプ実装)を表示するViewPagerの設定
     */
    private void setViewPager(){
        myPager = (ViewPager) findViewById(R.id.Q_question_ViewPager);
        myPager.setAdapter(new Question_PagerAdapter(getSupportFragmentManager(), q_id_data));
        myPager.setCurrentItem(currentId, false);
        myPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentId = position;
                setStateForSwipe();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * Buttonのクリックリスナーをセットする
     */
    private void setAnswerButtonsClickListener() {

        // 模擬試験モード以外の場合、採点アイコンは非表示
        FloatingActionButton actionButton = (FloatingActionButton)findViewById(R.id.fab_question_mark);
        if(mode_id != AiteaMode.TRIALEXAM) {
            actionButton.setVisibility(View.GONE);
        }else {
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClickMarkAlertshow();
                }
            });
        }

        // "ア"ボタン
        findViewById(R.id.Q_answerA).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_A);
                goNext_Activity();

            }
        });
        // "イ"ボタン
        findViewById(R.id.Q_answerI).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_I);
                goNext_Activity();
            }
        });
        // "ウ"ボタン
        findViewById(R.id.Q_answerU).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_U);
                goNext_Activity();
            }
        });
        // "エ"ボタン
        findViewById(R.id.Q_answerE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_E);
                goNext_Activity();
            }
        });
        // "？"ボタン
        findViewById(R.id.Q_answerQ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectAnswer = getString(R.string.question_answer_Q);
                goNext_Activity();
            }
        });
    }

    /**
     * ツールバーをセットする
     * 引数の値はタイトルの文字列に用いる
     */
    private void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_question);
        setNavigation(toolbar);
        toolbar.inflateMenu(R.menu.question);
        setToolbarText();

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    // メニュー内の"お気に入り"押下後の処理
                    case R.id.Q_favorite:
                        if (getFavoriteFlag(q_id_data[currentId])) {
                            item.setIcon(R.drawable.icon_favorite_false);
                            setFavoriteFlag(q_id_data[currentId], false);
                        } else {
                            item.setIcon(R.drawable.icon_favorite_true);
                            setFavoriteFlag(q_id_data[currentId], true);
                        }
                        break;

                    // メニュー内の"克服"押下後の処理
                    case R.id.Q_overcome:
                        if (getOvercomeFlag(q_id_data[currentId])) {
                            item.setIcon(R.drawable.icon_overcome_false);
                            setOvercomeFlag(q_id_data[currentId], false);
                        } else {
                            item.setIcon(R.drawable.icon_overcome_true);
                            setOvercomeFlag(q_id_data[currentId], true);
                            //アラート表示
                            ClickOvercomeAlertShow();
                        }
                        break;

                    // "次へ"ボタン
                    case R.id.menu_nextquestiion:
                        //次の問題へ
                        swipeNextQuestion();
                        break;
                }
                return true;
            }
        });
    }

    /**
     * ツールバーに表示するテキスト設定する
     */
    private void setToolbarText() {
        question_info_data qid = get_year_turn_qno(q_id_data[currentId]);
        ((TextView) findViewById(R.id.question_title)).setText(qid.getYear() + " " + qid.getTurn() + " 問" + qid.getQ_no());
    }

    private void setFlagIcon(String q_id) {
        if (getFavoriteFlag(q_id)) {
            ((Toolbar) findViewById(R.id.toolbar_question)).getMenu().findItem(R.id.Q_favorite).setIcon(R.drawable.icon_favorite_true);
        }else{
            ((Toolbar) findViewById(R.id.toolbar_question)).getMenu().findItem(R.id.Q_favorite).setIcon(R.drawable.icon_favorite_false);
        }
        if(getOvercomeFlag(q_id)) {
            ((Toolbar) findViewById(R.id.toolbar_question)).getMenu().findItem(R.id.Q_overcome).setIcon(R.drawable.icon_overcome_true);
        }else{
            ((Toolbar) findViewById(R.id.toolbar_question)).getMenu().findItem(R.id.Q_overcome).setIcon(R.drawable.icon_overcome_false);
        }
    }

    /**
     * 採点ボタン押下時のアラートダイアログ表示
     */
    private void ClickMarkAlertshow(){

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.alert_title_trial_exam_mark));
        alertDialogBuilder.setMessage(getString(R.string.alert_message_trial_exam_mark));
        alertDialogBuilder.setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //試験結果画面に遷移する
                goActivity_Question_Result();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.alert_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * 次の画面に遷移する処理
     */
    private void goNext_Activity(){

        //模擬試験モード
        if( mode_id == AiteaMode.TRIALEXAM ) {
            //db更新処理
            update_trial_exam_db();
            //次の回答画面へ遷移
            go_Next_Activity_Question();
        }else{
            //解説画面へ遷移
            goActivity_Question_CorrectAnswer();
        }
    }

    /**
     * 試験結果画面に遷移する
     */
    private void goActivity_Question_Result(){

        Intent intent = new Intent(Activity_Question.this, Activity_Trial_Exam_Result.class);
        startActivity(intent);
        finish();

    }
    /**
     * 次の問題の問題回答画面へ遷移する
     */
    private void go_Next_Activity_Question() {

        //全ての問題が回答済みの場合
        if ( isExistAllAnswerValue() ) {
            finish();
        }
        else{
            //次の問題にスライド
            swipeNextQuestion();
        }
    }

    /**
     * 次の問題に遷移する
     */
    private void swipeNextQuestion(){
        if((currentId + 1 == q_id_data.length )) {
            finish();
        }else {
            //次の問題にスライド
            currentId++;
            myPager.setCurrentItem(currentId, false);
        }
    }

    /**
     * 誤・回答日時・経過時間情報を取得し、模擬試験情報テーブルに反映する
     */
    private void update_trial_exam_db(){

        //経過時間の演算
        long elapsed_time = ( ( SystemClock.elapsedRealtime() - startTime ) / 1000 );

        //回答日時の取得
        String nowDate = Date.getNowDate();

        int correct_mistake = 0;
        //ユーザの回答と問題の正答が一致する
        if( selectAnswer.equals(get_correctAnswer(q_id_data[currentId])) ){
            correct_mistake = 1;
        }

        //模擬試験情報テーブル更新
        update_trial_exam_info(currentId + 1, selectAnswer, correct_mistake, nowDate, (int) elapsed_time);
    }

    /**
     * 解答画面に遷移
     */
    private void goActivity_Question_CorrectAnswer() {

        Intent intent = new Intent(Activity_Question.this, Activity_Question_CorrectAnswer.class);
        intent.putExtra(IntentKeyword.Q_ID_LIST, q_id_data);
        intent.putExtra(IntentKeyword.SELECT_Q_ID, currentId);
        intent.putExtra(IntentKeyword.USER_ANSWER, selectAnswer);
        intent.putExtra(IntentKeyword.MODE_ID, mode_id);
        startActivity(intent);
        finish();
    }

    /**
     * 模擬試験情報テーブルに全問題のユーザの回答が存在するかを判定する
     * @return 全ての問題が回答済み:true . 1件でも回答していない:false
     */
    private boolean isExistAllAnswerValue(){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_user_answer(), null);

        while (c.moveToNext()) {
            if( c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER)) == null ) {
                c.close();
                db.close();
                return false;
            }
        }

        c.close();
        db.close();

        return true;
    }

    /**
     * 引数で指定した問題IDに該当する問題回答画面を表示するのに必要なデータを取得する
     * @param q_id 問題ID
     * @return 問題回答画面を表示するのに必要なデータ
     */
    private question_info_data get_year_turn_qno(String q_id) {

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();
        db.beginTransaction();

        question_info_data out_q_i_data;

        Cursor c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_year_turn_qno(q_id), null);
        if(c.moveToNext()){
            out_q_i_data = new question_info_data();
            out_q_i_data.setYear(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.YEAR)));
            out_q_i_data.setTurn(c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.TURN)));
            out_q_i_data.setQ_no(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.Q_NO)));
        }else{
            return null;
        }
        c.close();

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        return out_q_i_data;
    }
    /**
     * 指定出題IDの模擬試験情報テーブルの回答・正誤・回答日時・経過時間を更新する
     * @param set_q_id			出題ID
     * @param user_answer		    回答
     * @param correct_mistake	正誤
     * @param user_answer_date    回答日時
     * @param elapsed_time		経過時間
     */
    private void update_trial_exam_info(int set_q_id,String user_answer ,int correct_mistake,String user_answer_date,int elapsed_time){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        db.execSQL(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.update_trial_exam_info(set_q_id, user_answer, correct_mistake, user_answer_date, elapsed_time));

        db.close();

    }

    /**
     * 問題情報から指定問題IDの正答を取得する
     * @param q_id	問題ID
     * @return 正答
     */
    private String get_correctAnswer(String q_id){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_correctAnswer(q_id), null);

        if (c.moveToNext()) {
            return c.getString(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.CORRECT_ANSWER));
        }
        c.close();
        db.close();
        return null;
    }


    /**
     * 模擬試験情報から指定出題IDの回答を取得する
     * @param set_q_id	出題ID
     * @return ユーザの回答
     */
    private String get_trial_exam_user_answer( int set_q_id){

        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_TRIAL_EXAM_INFO.get_trial_exam_user_answer(set_q_id), null);

        if (c.moveToNext()) {
            return c.getString(c.getColumnIndex(DB_ColumnNames.TRIAL_EXAM_INFO.USER_ANSWER));
        }
        c.close();
        db.close();
        return null;
    }

    /**
     * 指定問題IDのお気に入りフラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setFavoriteFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id,1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_favorite_flag(q_id,0));
        }

        db.close();
    }

    /**
     * 指定問題IDのお気に入りフラグを取得する
     * @param q_id 問題ID
     * @return お気に入りフラグ
     */
    private boolean getFavoriteFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_favorite_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.FAVORITE_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

    /**
     * 指定問題IDの克服フラグを設定する
     * @param q_id 指定問題ID
     * @param flag 設定値
     */
    private void setOvercomeFlag(String q_id, boolean flag){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        if(flag) {
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 1));
        }else{
            db.execSQL(DB_SqlMaker.TBL_QUESTION_INFO.set_overcome_flag(q_id, 0));
        }

        db.close();
    }

    /**
     * 指定問題IDの克服フラグを取得する
     * @param q_id 問題ID
     * @return 克服フラグ
     */
    private boolean getOvercomeFlag(String q_id){
        AiteaOpenHelper aiteaOpenHelper = new AiteaOpenHelper(this);
        SQLiteDatabase db = aiteaOpenHelper.getReadableDatabase();

        Cursor c = null;
        c = db.rawQuery(DB_SqlMaker.TBL_QUESTION_INFO.get_overcome_flag(q_id), null);

        if (c.moveToNext()) {
            if(c.getInt(c.getColumnIndex(DB_ColumnNames.QUESTION_INFO.OVERCOME_FLAG)) == 1){
                c.close();
                db.close();
                return true;
            }
        }
        c.close();
        db.close();
        return false;
    }

}
