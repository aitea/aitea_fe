package com.support.qualifications.aitea_fe.processing;

import com.support.qualifications.aitea_fe.database.genre_info_data;

/**
 * 大分類の正答率ソート用
 */
public class set_l_class_correct_rate_infoComparator implements java.util.Comparator {

    /** リターンコード */
    private int rc;

    /**
     * 1をセットすると昇順、-1をセットすると降順
     * @param rc
     */
    public set_l_class_correct_rate_infoComparator(int rc){
        this.rc = rc;
    }

    @Override
    public int compare(Object lhs, Object rhs) {
        if(((genre_info_data)lhs).getL_class_correct_rate() > ((genre_info_data) rhs).getL_class_correct_rate()){
            return rc;
        }
        else if(((genre_info_data)lhs).getL_class_correct_rate() < ((genre_info_data) rhs).getL_class_correct_rate()){
            return -rc;
        }
        return 0;
    }
}