package com.support.qualifications.aitea_fe.processing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.support.qualifications.aitea_fe.common.IntentKeyword;
import com.support.qualifications.aitea_fe.fragment.Fragment_Question;

/**
 * 問題画面のリストをスワイプで処理できるようにするためのアダプタ
 */
public class Question_PagerAdapter extends FragmentPagerAdapter {

    private String[] q_id_data;

    public Question_PagerAdapter(FragmentManager fm,String[] q_id_data){
        super(fm);
        this.q_id_data = q_id_data;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(IntentKeyword.CURRENT_Q_ID, q_id_data[position]);
        Fragment fragment = new Fragment_Question();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return q_id_data.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
